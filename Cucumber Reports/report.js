$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Include/features/Product.feature");
formatter.feature({
  "name": "Product",
  "description": "  User want to view product and sort product displayed",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@product"
    }
  ]
});
formatter.scenario({
  "name": "User want to sort product in descending",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@product"
    },
    {
      "name": "@tag1"
    }
  ]
});
formatter.step({
  "name": "User already on Product screen",
  "keyword": "Given "
});
formatter.match({
  "location": "Product.user_already_on_Product_screen()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User tap on sort button",
  "keyword": "When "
});
formatter.match({
  "location": "Product.user_tap_on_sort_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User tap on sort price descending",
  "keyword": "And "
});
formatter.match({
  "location": "Product.user_tap_on_sort_price_descending()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Product is sorted in descending way",
  "keyword": "Then "
});
formatter.match({
  "location": "Product.product_is_sorted_in_descending_way()"
});
formatter.result({
  "status": "passed"
});
});