package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import internal.GlobalVariable

public class Product {
	@Given("User already on Product screen")
	public void user_already_on_Product_screen() {
		Mobile.callTestCase(findTestCase('Step Definition/Login/User want to login using correct credential'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User tap on sort button")
	public void user_tap_on_sort_button() {
		Mobile.callTestCase(findTestCase('Pages/Products/Tap Filter'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@When("User tap on sort price descending")
	public void user_tap_on_sort_price_descending() {
		Mobile.callTestCase(findTestCase('Pages/Products/Tap Filter Descending'), [:], FailureHandling.STOP_ON_FAILURE)
	}

	@Then("Product is sorted in descending way")
	public void product_is_sorted_in_descending_way() {
		Mobile.callTestCase(findTestCase('Pages/Products/Sort Descending'), [:], FailureHandling.STOP_ON_FAILURE)
	}
}
