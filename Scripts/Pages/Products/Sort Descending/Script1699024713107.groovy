import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory as MobileDriverFactory
import io.appium.java_client.AppiumDriver as AppiumDriver
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import io.appium.java_client.MobileElement as MobileElement
import org.openqa.selenium.By as By

//AppiumDriver<?> driver = MobileDriverFactory.getDriver()
//def toast = driver.findElementByXPath("//android.widget.Toast[@text='"+ expectedError + "']")
//println("Toast element: " + toast)
//if (toast == null) {
//	KeywordUtil.markFailed('ERROR: Toast object not found!')
//}
driver = MobileDriverFactory.getDriver()

List price = []

for (int i = 1; i <= 2; i++) {
    List baris = driver.findElements(By.xpath(('//hierarchy/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.widget.ScrollView[1]/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup[' + 
            i) + ']/android.view.ViewGroup[1]/android.widget.TextView[3]'))

    for (MobileElement elemenBaris : baris) {
        oriPrice = elemenBaris.getText()

        trimmedPrice = (oriPrice[(1..oriPrice.length() - 1)])

        KeywordUtil.logInfo('trimmed price: ' + trimmedPrice)

        price.push(trimmedPrice)
    }
}

Mobile.verifyGreaterThan(price[0], price[1])

